export interface SaleAd {
    id : Number,
    title: String,
    description : String,
    price : Number  ,
    lastUpdated : Date,
    illustrations : []

}